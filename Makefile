phar:
	composer require php-yaoi/php-yaoi:^1;composer install --no-dev;rm -rf tests/;rm ./json-diff;rm ./json-diff.tar.gz;phar-composer build;mv ./json-diff.phar ./json-diff;tar -zcvf ./json-diff.tar.gz ./json-diff;git reset --hard;composer install

docker56-composer-update:
	test -f ./composer.phar || wget https://getcomposer.org/composer.phar
	docker run -v $$(pwd):/code php:5.6-cli bash -c "apt-get update;apt-get install -y unzip;cd /code;php composer.phar update --prefer-source"

docker56-test:
	docker run -v $$(pwd):/code php:5.6-cli bash -c "cd /code;php vendor/bin/phpunit"

update-test-base:
	docker build -t registry.gitlab.com/vearutop/test-pipe/ci-registry/test_base -f docker/test_image/base/Dockerfile .
	docker push registry.gitlab.com/vearutop/test-pipe/ci-registry/test_base

lsls:
	docker rm t2_1
	docker create -t --name=t2_1 --mount type=volume,target=/code registry.gitlab.com/vearutop/test-pipe/ci-registry/test_base
	docker run --rm --volumes-from t2_1 registry.gitlab.com/vearutop/test-pipe/php-builder ls -la

php-builder-alpine:
	docker build -t registry.gitlab.com/vearutop/test-pipe/php_builder_alpine docker/php_builder_alpine/
	docker push registry.gitlab.com/vearutop/test-pipe/php_builder_alpine
